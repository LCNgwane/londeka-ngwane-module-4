import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'Screens/welcome_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Welcome To ShyWay'),
      
      home: const SplashScreen(),
    );
    
  }
}


class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Column (
          children: [
            Image.asset('lib/assets/dasboard1.jpg'),
          ],
        ),
        splashIconSize: 5000,
        duration: 1000,
        backgroundColor: Colors.black,
        nextScreen: const WelcomePage()
      );
  }
